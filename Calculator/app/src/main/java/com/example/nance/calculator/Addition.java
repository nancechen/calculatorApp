package com.example.nance.calculator;

/**
 * Created by Nance on 12/05/2016.
 */
public class Addition extends Expression {
    Expression exp1;
    Expression exp2;

    public Addition(Expression exp1,Expression exp2){
        this.exp1=exp1;
        this.exp2=exp2;
    }

    @Override
    public String show() {
        return exp1.show()+" + "+exp2.show();
    }

    @Override
    public Double evaluate() {
        return exp1.evaluate()+exp2.evaluate();
    }
}