package com.example.nance.calculator;

/**
 * Created by willis on 17/05/16.
 */
public class Cos extends Expression {
    Double exp;

    public Cos (Double exp) {
        this.exp = exp;
    }

    @Override
    public String show() {
        return "cos(" + exp + ")";
    }

    @Override
    public Double evaluate() {
        return Math.cos(exp);
    }
}
