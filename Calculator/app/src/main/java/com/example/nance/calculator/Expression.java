package com.example.nance.calculator;

import java.util.HashMap;
import java.util.Objects;

/**
 * Created by Nance on 12/05/2016.
 */
public abstract class Expression {


    public abstract String show();

    public abstract Double evaluate();

    public static Expression parse(Tokenizer tok) {
        Object t = tok.current();
        if (t instanceof java.lang.Number) {
            Double v = ((java.lang.Number) t).doubleValue();
            tok.next();
            return parseHelper(new Number(v), tok);
        }
        else if (t.equals("(")) {
            tok.next();
            t = tok.current();
            if (t.equals("-")) {
                tok.next();
                t = tok.current();
                if (t instanceof java.lang.Number) {
                    tok.next();
                    tok.next();
                    Double v = ((java.lang.Number) t).doubleValue();
                    return parseHelper(new Unary(v), tok);
                } else throw new RuntimeException();
            }
            else if (t.equals("sin")) {
                tok.next();
                tok.next();
                t = tok.current();
                if (t instanceof java.lang.Number){
                    Double v = ((java.lang.Number) t).doubleValue();
                    tok.next();
                    tok.next();
                    Expression exp = new Sin(new Number(v));
                    return parseHelper(parseBracketHelper(exp, tok), tok);
                }
                else throw new RuntimeException();
            }
            else if (t.equals("cos")) {
                tok.next();
                tok.next();
                t = tok.current();
                if (t instanceof java.lang.Number) {
                    Double v = ((java.lang.Number) t).doubleValue();
                    tok.next();
                    tok.next();
                    Expression exp = new Cos(v);
                    return parseHelper(parseBracketHelper(exp,tok), tok);
                } else throw new RuntimeException();
            }
            else if (t.equals("tan")) {
                tok.next();
                tok.next();
                t = tok.current();
                if (t instanceof java.lang.Number) {
                    Double v = ((java.lang.Number) t).doubleValue();
                    tok.next();
                    tok.next();
                    Expression exp = new Tan(v);
                    return parseHelper(parseBracketHelper(exp,tok), tok);
                } else throw new RuntimeException();
            }
            else {
//                tok.next();
                return parseHelper(parseBracket(tok), tok);
            }
        } else if (t.equals("sin")) {
            tok.next();
            tok.next();
            t = tok.current();
            if (t instanceof java.lang.Number){
                Double v = ((java.lang.Number) t).doubleValue();
                tok.next();
                tok.next();
                return parseHelper(new Sin(new Number(v)), tok);
            }
            else throw new RuntimeException();
        }
        else if (t.equals("cos")) {
            tok.next();
            tok.next();
            t = tok.current();
            if (t instanceof java.lang.Number) {
                Double v = ((java.lang.Number) t).doubleValue();
                tok.next();
                tok.next();
                return parseHelper(new Cos(v), tok);
            } else throw new RuntimeException();
        }
        else if (t.equals("tan")) {
            tok.next();
            tok.next();
            t = tok.current();
            if (t instanceof java.lang.Number) {
                Double v = ((java.lang.Number) t).doubleValue();
                tok.next();
                tok.next();
                return parseHelper(new Tan(v), tok);
            } else throw new RuntimeException();
        }
        else throw new RuntimeException();
    }

    public static Expression parseHelper(Expression exp, Tokenizer tok){
        if (!tok.hasNext()){
            return exp;
        } else {
            Object t= tok.current();
            if (t instanceof String){
                String op = (String) t;
                tok.next();
                t = tok.current();
                if (t instanceof java.lang.Number) {
                    Double v = ((java.lang.Number) t).doubleValue();
                    tok.next();
                    switch (op) {
                        case "+":
                            return new Addition(exp, parseHelper(new Number(v),tok));
                        case "-":
                            return new Subtraction(exp,parseHelper(new Number(v),tok));
                        case "*":
                            return parseHelper(new Multiplication(exp,new Number(v)),tok);
                        case "/":
                            return parseHelper(new Division(exp,new Number(v)),tok);
                        case "%":
                            return parseHelper(new Mod(exp, new Number(v)),tok);
                    }
                }
                else if (t.equals("sin")) {
                    tok.next();
                    tok.next();
                    t = tok.current();
                    if (t instanceof java.lang.Number){
                        Double v = ((java.lang.Number) t).doubleValue();
                        tok.next();
                        tok.next();
                        switch (op) {
                            case "+":
                                return parseHelper(new Addition(exp, new Sin(new Number(v))), tok);
                            case "-":
                                return parseHelper(new Subtraction(exp, new Sin(new Number(v))), tok);
                            case "*":
                                return parseHelper(new Multiplication(exp,new Sin(new Number(v))),tok);
                            case "/":
                                return parseHelper(new Division(exp,new Sin(new Number(v))),tok);
                            case "%":
                                return parseHelper(new Mod(exp, new Sin(new Number(v))),tok);
                        }
                    }
                }
                else if (t.equals("cos")) {
                    tok.next();
                    tok.next();
                    t = tok.current();
                    if (t instanceof java.lang.Number){
                        Double v = ((java.lang.Number) t).doubleValue();
                        tok.next();
                        tok.next();
                        switch (op) {
                            case "+":
                                return parseHelper(new Addition(exp, new Cos(v)), tok);
                            case "-":
                                return parseHelper(new Subtraction(exp, new Cos(v)), tok);
                            case "*":
                                return parseHelper(new Multiplication(exp,new Cos(v)),tok);
                            case "/":
                                return parseHelper(new Division(exp,new Cos(v)),tok);
                            case "%":
                                return parseHelper(new Mod(exp, new Cos(v)),tok);
                        }
                    }
                }
                else if (t.equals("tan")) {
                    tok.next();
                    tok.next();
                    t = tok.current();
                    if (t instanceof java.lang.Number){
                        Double v = ((java.lang.Number) t).doubleValue();
                        tok.next();
                        tok.next();
                        switch (op) {
                            case "+":
                                return parseHelper(new Addition(exp, new Tan(v)), tok);
                            case "-":
                                return parseHelper(new Subtraction(exp, new Tan(v)), tok);
                            case "*":
                                return parseHelper(new Multiplication(exp,new Tan(v)),tok);
                            case "/":
                                return parseHelper(new Division(exp,new Tan(v)),tok);
                            case "%":
                                return parseHelper(new Mod(exp, new Tan(v)),tok);
                        }
                    }
                }
                else if (t.equals("(")){
                    tok.next();
                    t = tok.current();
                    if (t.equals("-")){
                        tok.next();
                        t = tok.current();
                        if (t instanceof java.lang.Number) {
                            tok.next();
                            tok.next();
                            Double v = ((java.lang.Number) t).doubleValue();
                            switch (op) {
                                case "+":
                                    return new Addition(exp, parseHelper(new Unary(v), tok));
                                case "-":
                                    return new Subtraction(exp, parseHelper(new Unary(v), tok));
                                case "*":
                                    return parseHelper(new Multiplication(exp, new Unary(v)), tok);
                                case "/":
                                    return parseHelper(new Division(exp, new Unary(v)), tok);
                                case "%":
                                    return parseHelper(new Mod(exp,new Unary(v)),tok);
                            }
                        }
                        else {
                            throw new RuntimeException();
                        }
                    }
                    else if (t.equals("sin")) {
                        tok.next();
                        tok.next();
                        t = tok.current();
                        if (t instanceof java.lang.Number){
                            Double v = ((java.lang.Number) t).doubleValue();
                            tok.next();
                            tok.next();
                            switch (op) {
                                case "+":
                                    return parseHelper(new Addition(exp, parseBracketHelper(new Sin(new Number(v)), tok)), tok);
                                case "-":
                                    return parseHelper(new Subtraction(exp, parseBracketHelper(new Sin(new Number(v)),tok)), tok);
                                case "*":
                                    return parseHelper(new Multiplication(exp, parseBracketHelper(new Sin(new Number(v)), tok)),tok);
                                case "/":
                                    return parseHelper(new Division(exp, parseBracketHelper(new Sin(new Number(v)), tok)),tok);
                                case "%":
                                    return parseHelper(new Mod(exp, parseBracketHelper(new Sin(new Number(v)), tok)),tok);
                            }
                        }
                    }
                    else if (t.equals("cos")) {
                        tok.next();
                        tok.next();
                        t = tok.current();
                        if (t instanceof java.lang.Number){
                            Double v = ((java.lang.Number) t).doubleValue();
                            tok.next();
                            tok.next();
                            switch (op) {
                                case "+":
                                    return parseHelper(new Addition(exp, parseBracketHelper(new Cos(v), tok)), tok);
                                case "-":
                                    return parseHelper(new Subtraction(exp, parseBracketHelper(new Cos(v),tok)), tok);
                                case "*":
                                    return parseHelper(new Multiplication(exp, parseBracketHelper(new Cos(v), tok)),tok);
                                case "/":
                                    return parseHelper(new Division(exp, parseBracketHelper(new Cos(v), tok)),tok);
                                case "%":
                                    return parseHelper(new Mod(exp, parseBracketHelper(new Cos(v), tok)),tok);
                            }
                        }
                    }
                    else if (t.equals("tan")) {
                        tok.next();
                        tok.next();
                        t = tok.current();
                        if (t instanceof java.lang.Number){
                            Double v = ((java.lang.Number) t).doubleValue();
                            tok.next();
                            tok.next();
                            switch (op) {
                                case "+":
                                    return parseHelper(new Addition(exp, parseBracketHelper(new Tan(v), tok)), tok);
                                case "-":
                                    return parseHelper(new Subtraction(exp, parseBracketHelper(new Tan(v),tok)), tok);
                                case "*":
                                    return parseHelper(new Multiplication(exp, parseBracketHelper(new Tan(v), tok)),tok);
                                case "/":
                                    return parseHelper(new Division(exp, parseBracketHelper(new Tan(v), tok)),tok);
                                case "%":
                                    return parseHelper(new Mod(exp, parseBracketHelper(new Tan(v), tok)),tok);
                            }
                        }
                    }
                    else {
                        switch (op) {
                            case "+":
                                return parseHelper(new Addition(exp, parseBracket(tok)), tok);
                            case "-":
                                return parseHelper(new Subtraction(exp, parseBracket(tok)), tok);
                            case "*":
                                return parseHelper(new Multiplication(exp, parseBracket(tok)), tok);
                            case "/":
                                return parseHelper(new Division(exp, parseBracket(tok)), tok);
                            case "%":
                                return parseHelper(new Mod(exp,parseBracket(tok)),tok);
                        }
                    }
                }
                else throw new RuntimeException("Number not provided");
            }else throw new RuntimeException("Operator not provided");
        }
        return null;
    }

    public static Expression parseBracket(Tokenizer tok) {
        Object t = tok.current();
        if (t instanceof java.lang.Number){
            Double v= ((java.lang.Number) t).doubleValue();
            tok.next();
            return parseBracketHelper(new Number(v), tok);
        }
        else if (t.equals("(")) {
            tok.next();
            t = tok.current();
            if (t.equals("-")){
                tok.next();
                t = tok.current();
                if (t instanceof java.lang.Number) {
                    Double v = ((java.lang.Number) t).doubleValue();
                    tok.next();
                    tok.next();
                    return parseBracketHelper(new Unary(v), tok);
                }
                else throw new RuntimeException();
            }
            else if (t instanceof java.lang.Number) {
                Double v= ((java.lang.Number) t).doubleValue();
                tok.next();
                return parseBracketHelper(new Number(v), tok);
            }
            else throw new RuntimeException();
        }
        else throw new RuntimeException();
    }

    private static Expression parseBracketHelper(Expression exp, Tokenizer tok) {
        Object t = tok.current();
        if (t.equals(")")){
            tok.next();
            return new Number(exp.evaluate());
        }
        else {
            if (t instanceof String) {
                String op = (String) t;
                tok.next();
                t = tok.current();
                if (t instanceof java.lang.Number) {
                    Double v = ((java.lang.Number) t).doubleValue();
                    tok.next();
                    switch (op) {
                        case "+":
                            return new Addition(exp, parseBracketHelper(new Number(v), tok));
                        case "-":
                            return new Subtraction(exp, parseBracketHelper(new Number(v), tok));
                        case "*":
                            return parseBracketHelper(new Multiplication(exp, new Number(v)), tok);
                        case "/":
                            return parseBracketHelper(new Division(exp, new Number(v)), tok);
                        case "%":
                            return parseBracketHelper(new Mod(exp,new Number(v)),tok);
                    }
                }
                else if (t.equals("(")) {
                    tok.next();
                    t = tok.current();
                    if (t.equals("-")) {
                        tok.next();
                        t = tok.current();
                        if (t instanceof java.lang.Number) {
                            tok.next();
                            tok.next();
                            Double v = ((java.lang.Number) t).doubleValue();
                            switch (op) {
                                case "+":
                                    return new Addition(exp, parseBracketHelper(new Unary(v), tok));
                                case "-":
                                    return new Subtraction(exp, parseBracketHelper(new Unary(v), tok));
                                case "*":
                                    return parseBracketHelper(new Multiplication(exp, new Unary(v)), tok);
                                case "/":
                                    return parseBracketHelper(new Division(exp, new Unary(v)), tok);
                                case "%":
                                    return parseBracketHelper(new Mod(exp, new Unary(v)), tok);
                            }
                        }
                        else throw new RuntimeException();
                    }
                    else {
                        switch (op) {
                            case "+":
                                return parseBracketHelper(new Addition(exp, parseBracket(tok)), tok);
                            case "-":
                                return parseBracketHelper(new Subtraction(exp, parseBracket(tok)), tok);
                            case "*":
                                return parseBracketHelper(new Multiplication(exp, parseBracket(tok)), tok);
                            case "/":
                                return parseBracketHelper(new Division(exp, parseBracket(tok)), tok);
                            case "%":
                                return parseBracketHelper(new Mod(exp,parseBracket(tok)),tok);
                        }
                    }
                }
            }
            else throw new RuntimeException("Operator not provided");
        }
        return null;
    }


    public static Expression parseold(Tokenizer tok) {

        Object t=tok.current();

        if (t instanceof java.lang.Number) {
            Double v = ((java.lang.Number) t).doubleValue();
            tok.next();
            if (!tok.hasNext()) {
                return new Number(v);
            } else {
                t = tok.current();
                if (t instanceof String) {
                    tok.next();
                    String op = (String) t;
                    switch (op) {
                        case "+":
                            return new Addition(new Number(v), parse(tok));
                        case "-":
                            return new Division(new Number(v), parse(tok));
                        case "*":
                            return new Multiplication(new Number(v),parse(tok));
                        case "/":
                            return new Division(new Number(v),parse(tok));
                        case "% ":
                            return new Mod(new Number(v),parse(tok));

                        /* case "(":
                            tok.next();
                            return ;
                        case ")":
                            return ;*/
                    }
                }
            }
        }else
            throw new RuntimeException();
        return null  ;
    }

    public static void main(String[] args) {

        Expression exp = Expression.parse(new Tokenizer("((1+1)+1)*2"));//new Addition(new Number(x), new Number(y));
        Double value = exp.evaluate();
        System.out.println(exp.show() + " evaluates to " + value);


    }
}
