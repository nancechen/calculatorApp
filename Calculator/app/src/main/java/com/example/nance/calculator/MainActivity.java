package com.example.nance.calculator;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.RequiresPermission;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    Button evaluateButton;
    EditText inputText;
    Button clearB;
    File saveFile;
    static String filename;
    FileInputStream fileInputStream;
    FileOutputStream fileOutputStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        inputText= (EditText) findViewById(R.id.inputText);
        evaluateButton=(Button) findViewById(R.id.calculate);
        clearB=(Button) findViewById(R.id.clearButton);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        filename = "calculate";
        saveFile = new File(getFilesDir(), filename);
        if (saveFile.exists()){
            try {
                fileInputStream = openFileInput(filename);
                BufferedReader input = new BufferedReader(new InputStreamReader(fileInputStream));
                String line;
                StringBuilder buffer = new StringBuilder();
                while ((line = input.readLine()) != null) {
                    buffer.append(line);
                }
                inputText.setText(buffer.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public void putExpresson(View view) {
        Editable str= inputText.getText();
        Button b = (Button)findViewById(view.getId());
        str = str.append(b.getText());
        inputText.setText(str);

    }

    public void clear(View view){

        String str="";
        inputText.setText(str);


    }
    public void evaluate(View view){
        String str=inputText.getText().toString();
        Expression exp = Expression.parse(new Tokenizer(str));
        Double value = exp.evaluate();
        String textBuffer = inputText.getText().toString();
        try {
            fileOutputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            fileOutputStream.write(textBuffer.getBytes());
            fileOutputStream.close();
            inputText.setText(textBuffer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputText.setText(value.toString());
    }
}
