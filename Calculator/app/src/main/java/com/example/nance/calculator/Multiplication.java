package com.example.nance.calculator;

import java.lang.*;

/**
 * Created by Nance on 14/05/2016.
 */
public class Multiplication extends Expression {
    Expression exp1;
    Expression exp2;
    public Multiplication(Expression exp1,Expression exp2){
        this.exp1=exp1;
        this.exp2=exp2;
    }

    @Override
    public String show() {
        return exp1.show()+" * "+exp2.show();
    }

    @Override
    public Double evaluate() {
        return exp1.evaluate() * exp2.evaluate();
    }
}

