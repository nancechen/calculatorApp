package com.example.nance.calculator;

/**
 * Created by Nance on 12/05/2016.
 */
public class Number extends Expression {
    Double number;

    public Number(Double number){
        this.number=number;
    }

    @Override
    public String show() {
        return ""+number;
    }

    @Override
    public Double evaluate() {
        return number;
    }
}
