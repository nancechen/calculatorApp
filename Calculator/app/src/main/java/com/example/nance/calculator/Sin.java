package com.example.nance.calculator;

/**
 * Created by willis on 16/05/16.
 */
public class Sin extends Expression {
    Expression exp;

    public Sin(Expression exp){
        this.exp = exp;
    }

    @Override
    public String show() {
        return "sin(" + exp.show() + ")";
    }

    @Override
    public Double evaluate() {
        return Math.sin(exp.evaluate());
    }


}
