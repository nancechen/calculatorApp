package com.example.nance.calculator;

/**
 * Created by Nance on 12/05/2016.
 */
public class Subtraction extends Expression {
    Expression number1;
    Expression number2;
    public Subtraction(Expression num1,Expression num2){
        this.number1=num1;
        this.number2=num2;
    }
    @Override
    public String show() {
        return number1.show() + " - " +number2.show();
    }

    @Override
    public Double evaluate() {
        return number1.evaluate()-number2.evaluate();
    }
}
