package com.example.nance.calculator;

/**
 * Created by willis on 17/05/16.
 */
public class Tan extends Expression {
    Double exp;

    public Tan(Double exp){
        this.exp = exp;
    }

    @Override
    public String show() {
        return "tan(" + exp + ")";
    }

    @Override
    public Double evaluate() {
        return Math.tan(exp);
    }
}
