package com.example.nance.calculator;

/**
 * Created by willis on 12/05/16.
 */
public class Token {
    public static void main(String[] args) {
        Tokenizer tokenizer = new Tokenizer("(-2.0)*5+1-6");
        Sin sin = new Sin(new Number(2.0));
        while (tokenizer.hasNext()){
            System.out.println(sin.evaluate());
            tokenizer.next();
        }
    }
}
