package com.example.nance.calculator;

import java.text.ParseException;

/**
 * Created by willis on 12/05/16.
 */
public class Tokenizer {
    private String inputString;
    private int position;
    private Object current;

    static final char whitespace[] = { ' ', '\n', '\t' };
    static final char operation[] = { '(', ')', '+' , '-', '*', '/','-'}; //last one is the unary operator

    public Tokenizer(String inputString) {
        this.inputString = inputString;
        this.position = 0;
        next();
    }

    boolean hasNext() {

        return current !=null;
    }

    Object current() {

        return current;
    }

    public void next() {
        consumeWhite();
        if (position == inputString.length()){
            current = null;
        }
        else if (isInString(inputString.charAt(position), operation)){
            current = "" + inputString.charAt(position);
            position++;
        }
        else if (Character.isDigit(inputString.charAt(position))){
            int start = position;
            while (position < inputString.length() && Character.isDigit(inputString.charAt(position))){
                position++;
            }
            if (position+1 < inputString.length() && inputString.charAt(position) == '.' && Character.isDigit(inputString.charAt(position+1))){
                position++;
                while (position < inputString.length() && Character.isDigit(inputString.charAt(position))) {
                    position++;
                }
                current = Double.parseDouble(inputString.substring(start, position));
            }
            else {
                current = Integer.parseInt(inputString.substring(start, position));
            }
        }
        else {
            int start = position;
            while (position < inputString.length() && !isInString(inputString.charAt(position), operation)&& !isInString(inputString.charAt(position),whitespace)){
                position++;
            }
            current = inputString.substring(start, position);
        }
    }

    private void consumeWhite() {
        while (position < inputString.length() && isInString(inputString.charAt(position), whitespace)){
            position++;
        }
    }

    private boolean isInString (char inputChar, char[] charArray){
        for (char x : charArray){
            if (x==inputChar){
                return true;
            }
        }
        return false;
    }

}
